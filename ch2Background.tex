\chapter{Background}
\label{BackGround}
\rhead{Background}
\thispagestyle{firststyle}

In this chapter, the authors present a detailed overview of Tor's design and system architecture.

\section{Tor Overview}

Tor provides a means to communicate over the Internet anonymously. A Tor client can remain anonymous from Internet servers, and the parties in communication can remain unlinked from each other from the perspective of an observer. The authors have discussed the mechanics of how this is achieved.
\\

The Tor network is composed of volunteer-operated nodes called \emph{Onion Routers(ORs)}, also known as \emph{relays} or \emph{nodes}. These \emph{ORs} provide network connectivity and bandwidth capacity for end-user traffic. Anyone may operate a relay and indeed a strength of Tor is the diversity and number of its network nodes. When an \emph{OR} joins the network it announces its details, such as its network address/port, its donated bandwidth capacity, and its \emph{exit policy} - stating to what Internet addresses and ports outside of the Tor network this relay is willing to send traffic - to the (distributed) \emph{directory authority}. The \emph{OR} will then be listed on the global list of relays and be a candidate for routing end user traffic. 
\\

An end user downloads the Tor client, also known as an \emph{Onion Proxy(OP)}, which on start up downloads the \emph{consensus} document of all running relays as well as relay descriptors from the \emph{directory authority} (or one of its mirrors). These documents contain the details of each relay that the \emph{OP} uses to route traffic through the network. In order to protect clients against route bridging and fingerprinting attacks\cite{danezis2008bridging}, these documents are updated hourly so as to provide a current and consistent picture of the network to all clients. \emph{Consensus} documents are published precisely once per hour and descriptors are updated in real time as their contents change.
\\

The \emph{directory authority} also provides metadata in the consensus document that helps the \emph{OP} route traffic more intelligently. In particular, the \emph{OP} uses the consensus in the process of constructing a \emph{circuit} - a path through the Tor network. By default, circuits consist of three \emph{ORs} selected by the \emph{OP}. The next section describes the process of router selection that is performed by \emph{OPs}.
\\

\textbf{Router selection}. In the default setting, the \emph{OP} selects \emph{ORs} from a distribution that favours higher-bandwidth relays but also allows low-bandwidth relays to be utilized to some extent. The three \emph{ORs} in the circuit are termed the \emph{entry}, \emph{middle}, and \emph{exit} \emph{ORs}. The \emph{OP} communicates directly with the \emph{entry OR}, the \emph{entry} communicates with the \emph{middle OR}, and the \emph{middle} communicates with the \emph{exit OR}. Finally, the \emph{exit OR} communicates directly with the destination Internet server.
\\

Although the number of circuits constructed is governed by immediate and anticipated need, a general rule is that each circuit is used for about ten minutes before the Tor client will begin using a fresh circuit.
\\

The \emph{OP} constructs the circuit as follows. The \emph{OP} first picks a suitable \emph{exit} relay - suitability being a function of the relay's configuration as an \emph{exit} relay(which is communicated to clients with the \emph{Exit} flag in the \emph{consensus} document) and its exit policy. Next, the \emph{OP} picks the entry \emph{OR} while ensuring that all the relays have distinct \emph{/16 IP addresses} and relay families\footnote[1]{Operators of multiple Tor relays can voluntarily mark all the \emph{ORs} they control as being in a common family.}. (We provide more details on the constraints placed on entry selection in Section \ref{SecEntryGuardRelays}.) The middle node is then picked in a similar fashion.
\\

Finally, the \emph{OP} constructs the circuit using the three \emph{ORs} in an incremental and telescoping manner. The \emph{OP} negotiates cryptographic material with the \emph{entry OR} and once an encrypted channel is established between them it asks the \emph{entry OR} to \emph{extend} the circuit towards the \emph{middle OR}. The \emph{OP} then negotiates cryptographic material with the \emph{middle OR} - communicating through the \emph{entry OR} - to establish an encrypted channel between them. The \emph{middle OR} is then asked to extend the circuit to the \emph{exit OR} and the process is repeated to establish a secure channel between the \emph{OP} and the \emph{exit} relay.


\section{Entry Guard Relays}
\label{SecEntryGuardRelays}

All Tor relays are donated and as such it is hard to know which ones can be trusted. It is easy, then, for the adversary to donate resources and participate in circuits. The danger is when the adversary controls both the \emph{entry} and \emph{exit ORs} on a single circuit. In this scenario the client address and destination address of the traffic are known to the adversary who, through tagging or traffic confirmation attacks\cite{danezis2005traffic,edman2009awareness,murdoch2007sampled}, effectively deanonymizes the client. Following this previous work, the authors assume that these attacks are easy and accurate to carry out; however, the extent to which this assumption is true is beyond the scope of this paper.
\\

Given enough time and the presence of adversarial \emph{ORs}, the \emph{OP} will eventually construct circuits that have malicious entry and exit \emph{ORs}. Since Tor picks relays weighted according to bandwidth, a sufficiently resourceful adversary can deluge the network with high-bandwidth relays and increase the rate at which it can compromise circuits.
\\

To mitigate this and related threats such as the predecessor attack\cite{wright2004predecessor} and locating hidden services\cite{overlier2006locating}, entry guards were introduced. They limit the impact an adversary can have on Tor's user base by effectively reducing the number of times each client selects her entry relays, thus slowing the rate of compromise and reach of the adversary.
\\

Instead of picking a new entry every time a circuit is constructed, the \emph{OP} maintains a \emph{guard list} of a handful of pre-selected entry relays. When the Tor client constructs this list, it selects an expiry time for each of the guards in the list uniformly at random from the range of 30-60 days; after that time, the guards will be dropped and repopulated, as described in detail below. When circuits are constructed, the \emph{entry relay} to be used is selected uniformly at random from the client's guard list. The rest of the circuit building process remains the same. The effect of this change is that if no malicious guard relays have been picked, the user is uncompromisable by the adversary until she picks new guards. The disadvantage is that if a client does pick a malicious guard then she has a higher probability of being compromised for the next 30-60 days. It is debatable if it is better to \textbf{a)} be compromised with some probability all the time or to \textbf{b)} be either completely safe, or else compromised with higher probability. \O{verlier} and Syverson\cite{overlier2006locating} provide analysis that the latter is preferable and hence the guard mechanism is embedded in the Tor client code.
\\

Moreover, since entry guards have the potential of negatively affecting the performance of the Tor network and security of its users, they need to be carefully selected. The main mechanisms in place are the directory authority, which assigns guard status to relays, and the guard selection algorithm executed by the Tor client. The next section explains how the \emph{guard} flag is obtained by \emph{ORs} and how the guard selection algorithm is carried out.
\\

\textbf{Guard flag}. All ORs in the Tor network are monitored for availability and bandwidth capacity by the directory authority. Relays deemed stable\footnote[2]{The Guard flag aims for high availability, not to be confused with the Stable flag from the consensus document, which is given to relays with above-median mean-time-between-failures.} and providing bandwidth above a certain threshold (currently the median of all relay bandwidths, or 250 KB/s, whichever is smaller\cite{TorSpecDirectoryProtocol3}) are selected to receive the guard flag in the consensus document; this flag marks a relay as eligible to be included in guard lists. This criterion promotes ORs that will most likely be around for a long time and provide a level of bandwidth that will not likely cause bottlenecks. However, the authors have found that there is large variance in actual guard bandwidth and stability. At the time of their experiments there were, on average, 800 routers with the guard flag. An important tension to note is that if the criteria are too selective, then few guards will be available, forcing more traffic through fewer nodes, at a cost to both network utilization and security. At the same time, if the criteria are too lenient, then less stable guards are likely to churn more often, leading to larger guard lists, and an increased likelihood of selecting a malicious guard. This report investigates this balance in detail.
\\

\textbf{Guard selection algorithm}. Each client ensures that the number of guards - both online and offline - in its guard list is at least the default number at all times. If a guard goes offline, either temporarily or permanently, and there are fewer than two online guards in the guard list, a new entry guard is picked, but each previous guard is retried periodically, with an increasing back-off period,\footnote[3]{While the authors do not analyze the effects of changing the back-off periods - currently believed to be orthogonal to Tor's guard design - COGS provides the ability to do so in the future.} according to \emph{Algorithm 1}. In addition, each of the relays in a client's guard list expires in 30-60 days as a guard rotation event occurs. The algorithm for picking a guard, in either scenario, is as follows:

\begin{itemize}
\item Read the consensus to find the set of relays with the guard flag set.
\item Exclude guards already in the client's guard list, if any.
\item Exclude guards in the same /16 IP block or family as any of the guards in the client's guard list.
\item Select a guard at random from the remaining list of relays, weighted by the relays' adjusted bandwidths (see below).
\item Assign a random expiration time 30-60 days hence.
\item Repeat until the guard list contains the required number of guard relays.
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{Algo1}
\label{Algo1}
\end{figure}

The adjusted bandwidths used as weights in the above algorithm are based on values reported in the consensus for each relay, further adjusted by utility weights. Since Tor's bandwidth capacity is at a premium, and exit bandwidth capacity more specifically, this weighting mechanism is in place to make the most of these resources, so that the network as a whole does not suffer from overly poor performance. These weights are a function of the total bandwidth of each relay type, the total network bandwidth, and the relative bandwidths and relay flags of individual relays. As the bandwidth and relay composition of the network changes, the bandwidth weights of individual relays also change. Note that the weighted consensus bandwidths are scalars without units; it is best to think of them as \emph{points}, where relays with more points are more likely to be chosen in circuits. They are not actual bandwidth measurements, and so it becomes difficult to translate this metric to real-world client experiences. The authors refer to these \emph{points} as Weighted Bandwidth Units(WBU).
\\

In general, exit bandwidth is protected such that relays with the \emph{Exit} flag are chosen in the exit position more than in other roles. In particular, guards that are also exits will find themselves used more often as exits and less often as guards. The authors have discussed the implications of this design choice in future chapters.
\\

\textbf{Threat model}. Tor provides anonymity properties against an adversary that has a limited visibility of the network. The adversary may operate malicious relays in the network and attain guard and exit flags by meeting the thresholds set out by the Tor specification. The goal of the adversary is to have relays under its control selected as the guard and exit relays on the same circuit, thus compromising the Tor user. The adversary does not have unlimited bandwidth and the authors count any relays it compromises as its own. Our investigation of guards is concerned with the choices for parameters made by the Tor community. These parameters are the guard rotation duration, which at present is set to a uniformly random time between 30 and 60 days, and the number of guards, which at present defaults to three.

\pagebreak