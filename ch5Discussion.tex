\chapter{Discussion}
\label{Discussion}
\rhead{Discussion}
\thispagestyle{firststyle}

In this chapter, the authors discuss the implications of their findings and address Dingledine's open research questions.
\\

\textbf{Guard stability and selection}. As guards are the first hop on circuits, all of Tor's functionality is contingent on their availability. Section \ref{NaturalChurn} shows that on the whole guards are quite stable. Compared to the general population of relays, guards are generally available for longer stretches of time and offline for shorter durations. This stability is a consequence of the guard flag assignment process governed by the directory authority and is as designed.
\\

\begin{table}[h]
\centering
\caption{\emph{Median guard bandwidth (WBU) from Apr-Nov 2011}}
\label{Table2}
\begin{tabular}{|c|c|c|c|}
\hline
\textbf{Min} & \textbf{Median} & \textbf{Mean} & \textbf{Max}\\[2mm] 
\hline
40 & 67 & 68.31 & 113 \\[1mm]\hline
\end{tabular}
\end{table}

However, this process is not perfect, as there are a large quantity of guards with a wide variety of stability characteristics that deviate from the intended entry guard design - recall Table \ref{Table1} for the range of downtimes and uptimes for guards. The authors note that the incidence of active guard lists with low average bandwidth in general is not prevalent; note that the curves for 3-10 guards in Figure \ref{Fig10} do not have long tails to the left of the median as compared to the 1 guard curve-meaning occasions where every guard in a client's active list has low bandwidth are rarer - and that perhaps the guard flag allocations could be more selective. Indeed, Table \ref{Table2} provides statistics on the median guard bandwidth during the 8-month time slice; it is calculated by finding the median WBU amongst all the guards in the consensus and then calculating the median WBU across all the consensuses. It shows that the greatest median guard relay bandwidth across all consensuses during that time slice is just 113 WBU, a level of active guard bandwidth which is surpassed by all clients with \emph{3G} or more and only suffered by 5\% of \emph{1G} clients(Figure \ref{Fig10}).
\\

The authors reason that since low-bandwidth guard lists are rare, low-bandwidth guards are not depended upon by end users and so removing them from guard lists will not have a big impact from a performance perspective. 
\\

However, it can be argued that for the sake of load balancing these low-bandwidth relays provide relief whenever the end user chooses them from their guard list instead of one of their higher-bandwidth guards. These nodes may also provide added security through additional relay diversity. It is unclear at the moment if these nodes actually fulfill these desired effects, and this direction is an avenue of future investigation.
\\

\textbf{Natural churn and its effects on client compromise}. In the lower curve in Figure \ref{Fig4}, it is clear that natural churn provides an adversary increased opportunities to compromise guard lists. The authors also note that although there is some downward pressure due to returning honest guards, the trend is upwards over time. If not for guard rotation, after a sufficient length of time a malicious relay should be able to compromise all client lists. Recall that while guard rotation speeds up the adversary's accumulation of clients initially, it is self limiting as the rate of clients gained equals the rate of clients lost due to churn.
\\

Furthermore, when reasoning about the impact of natural churn it is difficult to know beforehand when a guard is likely to return, if ever. It is due to this uncertainty that Tor uses such sensitive guard replacement policies and sophisticated retry mechanisms.
\\

Putting natural churn in perspective, the authors have reasoned that it is an artifact that cannot be removed from the network, and it has a large effect on the security and performance of the network. Therefore, the best policy may be to avoid situations that lead to churn in the first place by selecting guards more cautiously and mitigating the effects of churn when they do find one of our active guards offline.
\\

\textbf{Guard rotation}. Long-lived relays tend to accumulate clients over time, and malicious relays will remain online to take advantage of this effect. Rotating guards does in fact mitigate that eventuality.
\\

The authors note that guard rotation does, however, increase the chance of compromise: see the sudden increase in May 2011 - when guard rotation began to take effect in the experiment, 30 days after its beginning - of the curve in Figure \ref{Fig4}. The authors also note that in Figure \ref{Fig5} the number of guards a client is serviced by, and can hence potentially be compromised by, is much larger when guard rotation is in effect. Furthermore, Figure \ref{Fig9} indicates that all schemes expose clients to fewer guards when guard rotation is not enabled. As mitigation of the above, the authors could increase the minimum and maximum durations of guard rotation from the current 30-60 days and see a reduction in both metrics, since the frequency of rotation events would decrease.
\\

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{Fig11}
\caption{\emph{Guard longevity during Apr-Nov 2011}}
\label{Fig11}
\end{figure}

In order to reason about rotation durations and pick better ones the authors plot in Figure \ref{Fig11} the CDF of guard longevity for Apr-Nov 2011. They note that only about 9\% of guards remained part of the Tor network for the entire 8-month duration of the experiments. Also, the distribution is skewed towards shorter-lived routers with the median at 1371 hours. The current rotation period is between 720-1440 hours which means that the majority of guards undergo guard rotation. Since most guards are not long lived and leave the network of their own accord, guard rotation occurring as frequently as it currently does is both unnecessary and undesirable. We would prefer to target only those guards that are truly longer lived and thus are the cause for our concern, and ignore those that simply do not exist long enough to be worried about. Unfortunately, there is not an upward inflection point, apart from the two small ones at the extreme end of the time slice, which would indicate that longer-lived guards stand apart from the others and thus can be dealt with using a more appropriate rotation duration.
\\

Perhaps as an alternative for longer-lived, and potentially more-utilized guards, Tor ought to adjust their probabilities of being selected according to how long they have been part of the network, in addition to their bandwidths. The directory authorities can estimate the number of clients currently using each guard relay based on its historical weights and bandwidths, and adjust the weights in order to balance the load. In this manner, over time, each guard will effectively limit the load on itself as well as reduce the accumulation of clients.
\\

\textbf{Tor with one guard}. Intuitively, it seems that one guard ought to provide the best security but that perhaps performance would suffer. The results in Chapter \ref{Measurements} can be revisited to  evaluate this intuition. From a circuit compromise perspective, it can be seen in Figure \ref{Fig6} that Tor with one guard offers the least likelihood of compromised guard lists. The authors also note that fewer guards participate in an end user's guard list in that case (Figure \ref{Fig8}). However, from a performance perspective, in Figure \ref{Fig10} it can be noted that compared to Tor with three guards, Tor with one guard suffers from 60\% worse performance 50\% of the time but is better 50\% of the time where it provides 25\% more average guard list bandwidth. This outcome can be explained with guard lists that have a combination of slow and fast guards, which causes the average to be lower than the fastest guard. In the case of Tor with only one guard, when a fast guard is selected, the client can expect to receive fast service, provided that the middle and exit nodes are not slow. It is important to note, however, that Tor with one guard is superior to the other schemes evaluated in Chapter \ref{Measurements} when the bandwidths are already at acceptable levels, whereas it provides far slower performance at the lower ends of the bandwidth spectrum.
\\

Hence, the number of guards is a parameter that needs careful adjustment: the present results suggest that too few may lead to performance degradation, while more can have unnecessary security implications. Indeed, it may be the case that there are hereto undiscovered security implications of using just one guard as well. 
\\

\textbf{Answers to research questions}. The authors now tie the results of the investigation to the questions posed in Chapter \ref{Introduction}, and see how much progress has been made and what remains to be answered.
\\

The authors found that adversarial bandwidth is directly related to client compromise rates and that connection this is an unavoidable effect of favouring higher bandwidth relays - recall that this is a design choice, for the sake of better performance. What is interesting is that, as seen in Figure \ref{Fig2}, bandwidth and compromise rates are not linearly related. While more research is required to establish the exact relationship, it is clear that performance-enhancing measures have led to higher client compromise rates in this regard.
\\

The authors found that users achieve greater security if they reduce the number of entry guards that is used. They can further improve their security by eliminating or reducing the process of guard rotation. However, they also found that the security improvements through the reduction of guards and guard rotation come at the expense of performance degradation. They also found that natural churn, while inherent in the network and a source of compromise, works to amplify the compromise rate, but is not a dominating factor in the present Tor network.
\\

The authors did not vary guard rotation periods in this work. In future work the authors plan to explore a mechanism that is independent of relay stability and network characteristics. 
\\

By putting all the parameters together the authors find in general that if a suitable alternative to guard rotation can be found and smaller guard lists used, then the security of Tor's users will increase significantly while the impact to performance for clients with slower-than-average guards will degrade only slightly.
\\

This risk could be further mitigated by making the guard flag more selective and thus removing low-bandwidth guards, which would raise the average guard bandwidth for all clients. The authors identify this proposal as an area for further analysis.

\pagebreak