\select@language {english}
~\hfill \textbf {Page}\par 
\contentsline {chapter}{Abstract}{ii}
\contentsline {chapter}{List of Figures}{v}
\contentsline {chapter}{List of Tables}{vi}
\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}Introduction to WiFi: IEEE 802.11}{1}
\contentsline {section}{\numberline {1.2}Introduction to Ethernet: IEEE 802.3}{2}
\contentsline {section}{\numberline {1.3}A Comparison of WiFi and Ethernet:}{3}
\contentsline {subsection}{\numberline {1.3.1}Connections}{3}
\contentsline {subsection}{\numberline {1.3.2}Coverage}{4}
\contentsline {subsection}{\numberline {1.3.3}Security}{4}
\contentsline {subsection}{\numberline {1.3.4}Speed/Rate}{5}
\contentsline {subsection}{\numberline {1.3.5}Latency}{5}
\contentsline {section}{\numberline {1.4}Interoperability Between WiFi And Ethernet:}{6}
\contentsline {section}{\numberline {1.5}Organization Of The Report}{7}
\contentsline {chapter}{\numberline {2}Literature Survey}{10}
\contentsline {chapter}{\numberline {3}Modeling and Design}{14}
\contentsline {section}{\numberline {3.1}Motivation:}{14}
\contentsline {section}{\numberline {3.2}Simple Client-Server Model:}{14}
\contentsline {section}{\numberline {3.3}Berkeley Sockets:}{15}
\contentsline {section}{\numberline {3.4}Problem Statement}{18}
\contentsline {section}{\numberline {3.5}Technological Background}{19}
\contentsline {subsection}{\numberline {3.5.1}Ubuntu}{19}
\contentsline {subsection}{\numberline {3.5.2}Python}{20}
\contentsline {subsubsection}{Features}{20}
\contentsline {subsection}{\numberline {3.5.3}Twisted Python}{21}
\contentsline {subsubsection}{Core Ideas}{23}
\contentsline {subsubsection}{Event Driven Programming}{24}
\contentsline {subsubsection}{Twisted Modules And Framework}{25}
\contentsline {subsection}{\numberline {3.5.4}Wireshark}{28}
\contentsline {subsubsection}{Packet Sniffer}{28}
\contentsline {subsubsection}{Getting And Running Wireshark}{28}
\contentsline {subsection}{\numberline {3.5.5}Network Interfaces}{29}
\contentsline {subsubsection}{Ethernet}{29}
\contentsline {subsubsection}{WiFi}{31}
\contentsline {chapter}{\numberline {4}Implementation and Analysis}{33}
\contentsline {section}{\numberline {4.1}Client-Server Communication}{33}
\contentsline {section}{\numberline {4.2}The Twisted Modules Used}{34}
\contentsline {section}{\numberline {4.3}Algorithms}{35}
\contentsline {subsection}{\numberline {4.3.1}Double Interface Connection}{38}
\contentsline {subsection}{\numberline {4.3.2}Single Interface Connection}{39}
\contentsline {section}{\numberline {4.4}Working Procedure}{41}
\contentsline {section}{\numberline {4.5}Performance Analysis}{42}
\contentsline {chapter}{\numberline {5}Conclusions and Future Work}{45}
\contentsline {chapter}{Appendix \numberline {A}IEEE Publication}{47}
\contentsline {chapter}{Bibliography}{49}
