\chapter{COGS Framework}
\label{COGS}
\rhead{COGS Framework}
\thispagestyle{firststyle}

The design of the COGS framework is guided by Tor's guard path selection design, its governing parameters, the historical data sets available, and the research questions that the authors would like to answer. The design is extensible in that future research questions pertaining to guard and path selection can also be investigated using the same framework with minimal effort.
\\

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{Fig1}
\caption{\emph{COGS framework}}
\label{FigCOGSFramework}
\end{figure}

The framework encompasses \textbf{a)} researcher-defined observables or run-time measurements, \textbf{b)} the data sets available from the Tor Metrics Portal\cite{Torperf}, \textbf{c)} a Tor client simulator with hooks into the internal running state of thousands of simulated clients, \textbf{d)} configuration files that instrument the simulator for each experiment, and \textbf{e)} log parsers for data aggregation and statistics. Figure \ref{FigCOGSFramework} provides a graphical representation of the framework. The authors have described each in turn next. 
\\

\textbf{Observables}. In order to drive the analysis and produce justifiable answers to the questions posed earlier, the authors define the following \emph{observables} - metrics, attributes and effects that they have measured. The authors have outlined more observables for further research in Chapter \ref{Conclusion}. 
\\

From the historical consensus and descriptor documents the authors have picked observables that will shed light into the behaviour of guards. The authors focus on client compromise and how it is affected by natural churn and the operational parameters chosen by the Tor community.
\\

The pattern of up-time and down-time for each relay provides insight into its \emph{stability}. Using the consensus history the authors have measured the consecutive down-times of each relay; the same is done for up-times. From this the authors have calculated the mean time to recover between two runs of up times as well as the mean time between failure between two runs of down times. Statistical analysis provides the average case for the general population of relays and that of guards.
\\

To measure the impact of guard selection the authors have also recorded the \emph{number of guards that observe each client during the simulation}. This indicator is useful since it establishes the high water mark of potential compromise for each client. Even though each guard may have only been an active guard to a client for a short period of time, it is not safe to assume that the short period afforded limited impact on the client's privacy, since that short period may have been very sensitive in nature.
\\

Additionally, the authors have measured the number of clients at each consensus for whom at least one malicious guard is in the \emph{active guard list}; the authors have termed this event \emph{guard list compromise}. The active guard list is the first \emph{N} online relays in the client's guard list ordered by age, where \emph{N} is the number of entry guards being utilized by the client. This metric provides a view from the adversary's perspective of how many clients it could potentially compromise at any given time. Whereas Tor will always maintain a minimum of two online guards, we experiment with active guard lists that at times shrink to one in Chapter \ref{Measurements}.
\\

Finally, to evaluate the effect on performance of reduced active guard lists that may occur due to changes to Tor's default behaviour, the authors have measured the occurrences of active guard lists whose average bandwidth falls below a certain threshold. The number of active guards is not as important here as the average of their bandwidths, since this value can directly influence the client's expected performance. The authors have measured the average active guard list bandwidth as an indicator of the end user's experience and not as an expectation of the performance of any particular circuit. Recall also, from Section \ref{SecEntryGuardRelays}, that the weighted consensus bandwidths do not represent absolute bandwidths; nonetheless, it can be used to meaningfully compare the schemes against each other to find the relative merits of each.
\\

\textbf{Data sets}. The Tor Metrics Portal\cite{Torperf} provides hourly snapshots of publicly download-able Tor relay descriptors and actual published consensus documents from mid-2007 to the present. This data offers a glimpse into the state of the Tor network over the past several years in terms of the total number of relays, their flags, and their bandwidths. In addition, the presence (or absence) of any particular relays enables to analyze relay stability over time.
\\

\textbf{Configuration files of run-time options}. The behaviour of Tor clients, the adversary's attributes, and the network characteristics can be changed by passing parameters at run time through configuration files. Many experiments can be run simultaneously and independently - contingent on compute and storage resources - to provide insights into the behaviour of stock Tor and the many interesting variations that research questions introduce. This mechanism allows us to attain answers in an efficient and reproducible manner. The authors have discussed their parameter choices in more detail further sections.
\\

\textbf{Tor path selection simulator}. Using the publicly available data sets and the selected observables, the authors have constructed a Tor path selection simulator that selects guard relays and generates paths for a large number of simulated Tor clients. The simulator takes two pieces of data and a configuration file as input:
\begin{enumerate}
\item \textbf{Consensus documents}: The simulator reads unmodified consensus documents, one at a time, over the course of the time period desired. The consensus provides information such as each relay's bandwidth weighting and its flags.
\item \textbf{Relay descriptors}: The simulator also reads in relay descriptors that correspond to each relay listed in a particular consensus to allow correct Tor client behaviour.
\item \textbf{Run-time options}: The simulator takes run-time parameters to introduce malicious relays(if an adversary is modeled), augment the behaviour of clients(if required), choose the number of clients to be simulated, and produce logs of the observables.
\end{enumerate}

In order to ensure the highest possible level of fidelity to Tor's design, the simulator is based on Tor's original source code (version 0.2.2.33). For each consensus period, the simulated clients select or update their guard lists, following all of the Tor rules for guard replacement as described in Section \ref{SecEntryGuardRelays}.
\\

The simulator allows to control the guard rotation mechanism built in to Tor to test the effects of various guard rotation durations(or lack of them) on client compromise and also allows to investigate the effects of client guard list size.
\\

The granularity of the simulations is one hour, which corresponds to the granularity of the consensus documents. Every consensus lists the relays that were available at the time; they are loaded into the memory of the simulator, which then proceeds to select guard relays according to Tor's procedure for every client. These guards are written to a log file for later processing. Each consensus is fed into the simulator as a means to walk through time and produce guard selection scenarios. It uses parameter settings provided by us to simulate different network characteristics such as the number of guards, guard rotation period, and others. Where consensuses are missing from the Tor Metrics dataset, the simulator skips that hour of history but all time-sensitive rules and operations are followed and are reflected in the simulation results.
\\

An adversary with a fixed budget of relay bandwidth can be simulated by injecting it into the list of routers in each consensus period. The adversary is modeled by the amount of bandwidth it owns and the number of nodes it controls.
\\

The authors have also instrumented the Tor client code to log client state to disk for all observables they are interested in. However the authors refrained from logging all state changes due to storage constraint considerations. 
\\

The authors have made COGS available as open-source software and it is available from http://crysp.uwaterloo.ca/software.	
\\

\textbf{Simulation setup and parameter choices}. The simulations were run on multi-core servers to take advantage of parallelism in the experiments. Each simulation run introduced 80,000 clients\footnote[4]{This sample size was chosen by conducting experiments of increasing sizes and finding the point at which the resulting distributions stabilize, according to the Kolmogorov-Smirnov distance.}.
\\

It is not yet clear how to best model the client behaviour as there is yet no consensus within the Tor community on real-world client behaviour. Indeed, this is a research problem in itself and out of the scope of this work. Therefore, the authors have modeled the user base size as constant with no new clients joining the network, since the simulations focus on long-term effects that are not sensitive to user churn. For simplicity the simulated clients are always online, which is a worst-case scenario since live clients do not use Tor continuously.
\\

The authors choose the duration of the simulation by providing the starting and ending epoch times. The authors chose Apr 2011 - Nov 2011 as the target time slice since it has relatively stable bandwidth characteristics and a consistent consensus version number.
\\

COGS allows the injection of malicious routers into the network at run time through a configuration parameter\footnote[5]{While this paper investigates only one value of this parameter, it is simple to instantiate other behaviours through other values.}. The authors have chosen to introduce the malicious relay one consensus period, i.e. one hour, after the simulation has begun and all clients already have honest guards in their lists. This simulates an adversary attacking Tor after clients have already started using it and also establishes more conservative compromise rates - effectively a lower bound. For the simulations the authors assigned  malicious relay the guard flag only since also having the exit flag reduces the probability of a router being picked as a guard relay and would confound our results. Note that the choice to operate an exit node is with the relay operator and is not controlled by any authority.
\\

The bandwidth assigned to this relay, approximately in the top 20\% of guards, is incorporated into the network using the same rules and bandwidth weightings as the normal routers. Using the results from Murdoch and Watson\cite{murdoch2008metrics}, the authors have only introduced one malicious relay because Tor's guard selection algorithm chooses guards in proportion to their bandwidth; this design means that an adversary operating one high-bandwidth relay is equivalent to one operating many low-bandwidth relays as long as the total bandwidths are the same. Since the authors considered the adversary to be intelligent and capable of leveraging any and every advantage, the authors consider a client to be compromised if even one malicious guard exists in her active guard list.
\\

It should be noted that while the authors initially set malicious bandwidth as a proportion of the total bandwidth, this proportion changes over time along with the total network bandwidth. The authors have reasoned that keeping this value constant does not harm the experiments since \textbf{i)} a real adversary would not measure bandwidths on the entire network to keep malicious bandwidth proportions constant and \textbf{ii)} the bandwidth variance is small in the selected time period.
\\

\textbf{Log parser and data visualization}. The log parsers - there are several variants depending on the observables - extract the data that the authors are most interested in and compile it into a format that can then be fed into data visualization programs. The data can be processed by a variety of parsers in order to gain insight into various aspects of guard design.
\\

The existing parsers process the raw logs to provide data on compromise rates, total guard exposure over the experiment run and expected client performance.
\\

While COGS is rooted in guard analysis, it can also be used to simulate other Tor-related phenomena that do not involve actual network traffic. Examples include the analysis of client circuit diversity, the effects of introducing exit guards, and assessing whole-network effects of heterogeneous client configurations.

\pagebreak
