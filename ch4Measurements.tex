\chapter{Measurements and Evaluation}
\label{Measurements}
\rhead{Measurements and Evaluation}
\thispagestyle{firststyle}

In this chapter, COGS is used to collect the empirical data that will be used to answer the four open research questions introduced earlier. The main aim is to understand the effects of various guard design choices on compromise rates. The authors have measured the frequency with which a client picks new guards, since the more often guards are picked the more often a malicious relay has the chance to be placed in the client's guard list. The two main influences on the frequency of guard selection - other than a new client joining the network - are natural churn and guard rotation. The authors have measured and evaluated the characteristics of each in order to better understand the threats to client privacy.
\\

\textbf{Adversarial bandwidth and compromise rates}. The authors have based the subsequent analysis on the assumption that malicious bandwidth is directly related to compromise rates, albeit in complex ways. As the adversary increases their bandwidth contribution they are able to compromise more clients. This result is by design, as the Tor guard selection algorithm favours relays with higher bandwidth. The authors confirm this assumption in Figure \ref{Fig2}, which shows that as the malicious bandwidth increases the compromise rates also increase.
\\

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{Fig2}
\caption{\emph{Client compromise rates at various adversarial bandwidths, where WBU is the amount of weighted bandwidth units assigned to the malicious relay.}}
\label{Fig2}
\end{figure}

Since relay bandwidth is independent of the other variables under study, the authors have kept the malicious relay's bandwidth constant at 100 WBU for the rest of the experiments.
\\

\begin{table}[h]
\centering
\caption{\emph{Up and down times, in hours, of guard relays for Apr-Nov 2011}}
\label{Table1}
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
& \textbf{Min} & \textbf{$1^{st}$Qu.} & \textbf{Median} & \textbf{$3^{rd}$Qu.} & \textbf{Mean} & \textbf{Max}\\[2mm] 
\hline
\textbf{Guard Down} & 1 & 1 & 3 & 11 & 42.17 & 4978 \\[1mm]
\textbf{Guard Up} & 1 & 7 & 20 & 127 & 156.7 & 3829 \\[1mm]
\textbf{All Down} & 1 & 3 & 10 & 20 & 45 & 5454 \\[1mm]  
\textbf{All Up} & 1 & 1 & 4 & 11 & 19.82 & 3829 \\[1mm]\hline
\end{tabular}
\end{table}


\section{Natural Churn}
\label{NaturalChurn}
To measure the effect of natural churn, the authors start by first analyzing the consensus data and establishing the pattern of churn(e.g., up and downtimes) for each relay over time. The subsequent statistical analysis provides the results in Table \ref{Table1}. Note that the authors allow for the effects of relays that had a high frequency of up/down events, and that only relays that were available April to November 2011 were included in the data set.
\\

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{Fig3}
\caption{\emph{Router up and down times for all routers and for guards alone}}
\label{Fig3}
\end{figure}

The distance between the upper and lower curves in Figure \ref{Fig3} indicates that guards are more stable compared to the general router population, due to their longer uptimes and shorter downtimes. 
\\

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{Fig4}
\caption{\emph{Effects of natural churn and guard rotation on active guard list compromise}}
\label{Fig4}
\end{figure}

Next, the authors measure the effect of natural churn on guard list compromise and present the results in Figure \ref{Fig4} as the lower curve. For this analysis the authors have removed the normal guard rotation mechanism in the Tor client to isolate the effects of natural churn. The authors note that natural churn occurs frequently and also has a large effect on the network as indicated by the large uptick in compromised guard lists over time. The sharp peaks and valleys between May 1, 2011 and June 30, 2011 are indicative of honest guards that go down briefly - during which time our malicious guard has an opportunity to move into the active guard list - and then return - which bumps the malicious guard out of the active list again. These characteristic short guard down times concur with both Table \ref{Table1} and Figure \ref{Fig3}.
\\

From the upward trend of the curve it is known that natural churn has a real and lasting effect on client security and increases with time. Given enough time a long-lived adversary will appear in all clients' guard lists. This risk can be mitigated with periodic guard rotation, which is presented next.
\\

\section{Guard Rotation}
The second factor to guard list compromise is the mechanism to rotate each client's guards after defined periods of time. By default a Tor client drops its guards that are between 30-60 days old in the guard list. There are two major reasons: to limit the number of clients a single well-resourced guard can service, and hence compromise, at any given time and to balance the load so that long-serving guards do not potentially end up bearing the load of more clients over time. A negative effect is that clients with all honest guards are exposed to potentially selecting a malicious guard upon rotation, thus ensuring that after enough time all clients will have been compromised at some point.
\\

It is difficult to isolate the effects of guard rotation from those of natural churn under simulation with real data. The authors, however, have analyzed the effects of guard rotation in closed form and also analyze the empirical results of the additional effect of guard rotation to natural churn in simulation.
\\

During the target time slice of eight months, the authors expected that every client will rotate their guards at most as often as 30 days and at least as often as every 60 days. The maximum number of potentially unique guards that a client selects in those eight months is therefore 24, the minimum is 12, and the average is 17. This value is the number of guard relays that can potentially compromise the client. Note that without guard rotation, the least number of guards per client would be three.
\\

The upper curve in Figure \ref{Fig4} shows the additional effect that guard rotation has on compromise rates. In the first 30 days a steady increase on both curves in compromise rates can be seen as only natural churn is in effect. Then between 30-60 days the guard rotation really begins to show its effects in the upper curve, peaking at the end of May after which point a steady state seems to have been reached, where the amount of new compromised active lists is offset with losses in compromised active lists. The upward and downward trends are in part due to the malicious relay being pushed out of the active guard list by honest relays returning from a downtime.
\\

It is obvious that guard rotation increases the chances of active guard list compromise substantially. This result implies that guard rotation has a larger effect on compromise than does natural churn alone. Although it is difficult to isolate the interplay of natural churn and guard rotation it is simple to see that guard rotation does have negative effects.
\\

A key takeaway here is that the nature of guard rotation and natural churn are different, which explains the disparity between the curves. Guard rotation replaces a guard, while natural churn only provides a backup guard. If the client picks no malicious guards(as is the case initially), then with only natural churn in effect the malicious relay can only hope to be picked once a client's guard goes offline. However, it will never be at the top of the list and will be bumped out of the active list once the original guard returns. On the other hand, when guard rotation is used, every 30-60 days the malicious relay has a chance to be picked as one of the first three guards, thus cementing its place in the active guard list and thereby enabling potential compromise whenever it is used.
\\

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{Fig5}
\caption{\emph{Comparison of natural churn and guard rotation effects on clients' exposure to guards}}
\label{Fig5}
\end{figure}

Figure \ref{Fig5} shows the fraction of clients that have been seen by various numbers of guards for Tor with and without guard rotation. Guard rotation increases the visibility of each client on average to 19 guards. Recall that rotation causes at least 15 guards to see the client at minimum, so coupled with natural churn this effect is amplified. The effects of natural churn alone are small according to this metric: the mean increases to five versus the minimum three guards per client as indicated by the left curve.
\\

As a counterpoint the authors observe that guard rotation does serve a beneficial purpose. As mentioned earlier, it reduces the likelihood that certain long-lived guards will accumulate a large set of clients and hence potentially compromise them. This self-limiting nature means that it is not desirable to remove guard rotation as a mechanism without a suitable alternative; the authors are actively exploring this area as ongoing work.
\\

\section{Guard List Size}

Next, the authors investigate the effects of the size of the client's guard list and provide results and analysis for various values. The authors include results both with and without guard rotation enabled. For these experiments the authors run independent simulations for each of the guard list size settings so the clients are homogeneous within each run.
\\

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{Fig6}
\caption{\emph{Client compromise rates at various client guard list sizes, with guard rotation}}
\label{Fig6}
\end{figure}

Recall that the client will only replace a guard if guard rotation dictates it(if in effect) or supplement it when there are fewer than two guards online from the client's guard list. Figure \ref{Fig6} shows client compromise rates with guard rotation when the size of the client's guard list is 1, 2, 3, 5 and 10 guards, where the `G' stands for guards. From this analysis we discover that increasing the size of the guard list increases the client compromise rates. 
\\

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{Fig7}
\caption{\emph{Client compromise rates at various client guard list sizes, without guard rotation}}
\label{Fig7}
\end{figure}

However, compare these rates to the results without guard rotation in Figure \ref{Fig7}, where the absolute compromise rates are far lower but steadily increase over time. Also note that with guard rotation off, increasing the guard list size beyond 3 guards has the reverse effect of decreasing client compromise(curves for 5 and 10 guards). However, this effect does not last. The authors see the curve for 5 guards crossing over the 1 guard curve, with all indications of eventually crossing over the 2 and 3 guard curves as well if the upward trend continues. The same trend occurs for the 10 guard curve. The reason behind this trend is that initially the pool of possible guards is large and all are online; as guards fail, the client does not take any steps to replace them since the size of the guard list is still large enough and at least two of them are online. As the guards that failed are removed from the list, more guards are picked to maintain the overall size of the client's guard list. This last effect slowly erodes the advantage of starting off with a large pool of guards.
\\

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{Fig8}
\caption{\emph{Client guard exposure with guard rotation at various guard list sizes}}
\label{Fig8}
\end{figure}

The authors now consider the number of guards seen over time for different starting guard list sizes. Figure \ref{Fig8} shows the effect of increasing guard list size on clients' guard exposure. It is apparent that increasing the guard list size increases the client's guard exposure. 
\\

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{Fig9}
\caption{\emph{Client guard exposure without guard rotation at various guard list sizes}}
\label{Fig9}
\end{figure}

Figure \ref{Fig9} provides results for when guard rotation is turned off. While the overall guard exposure is far less than when guard rotation is in effect, the authors see the same trend where larger starting guard list size equates to more guard exposure. The authors observe that as the client guard list size increases, the probability of more guards ever being added to the list decreases. This effect is particularly striking for the 10 guard curve, and also evident for the 5 guard curve. This result is due to relative guard stability and also to the condition that fewer than two guards be present before a new guard is added. Comparing both figures the authors see a more general trend that without guard rotation the value of guard exposure is close to the starting guard list size(more pronounced for higher values) whereas with guard rotation the values of guard exposure are many times larger.
\\

\section{Available Bandwidth}


\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{Fig10}
\caption{\emph{Client's expected circuit performance with guard rotation at various guard list sizes. Performance results without guard rotation are nearly identical}}
\label{Fig10}
\end{figure}

Before the authors can make any conclusions we must look at the effects of these parameters on the average available bandwidth a client's guards provide it. Figure \ref{Fig10} shows the expected bandwidth for a client circuit. Results with and without guard rotation are nearly identical with negligible variations meaning that average performance is independent of guard rotation.
\\

Recalling that higher-bandwidth guards are more likely to be selected for spots in a client's guard list, poor guard bandwidth availability happens when all of a client's active guards have low bandwidth. This situation occurs with decreasing probability as the number of active guards increases, as is reflected in the dramatic decrease in the long left tail in Figure \ref{Fig10} as the number of guards increases from 1 to 3. Above 3, however, the improvements are less pronounced.

\pagebreak
